# Use an official Node.js runtime as a parent image
FROM node:14

# Set the working directory in the container to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install Uptime Kuma
RUN npm install uptime-kuma

# Make port 3001 available to the world outside this container
EXPOSE 3001

# Define environment variable
ENV NAME UptimeKuma

# Run Uptime Kuma when the container launches
CMD ["npm", "start"]
